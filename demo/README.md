# Class Assignment 2 - Part 2

## Goals
Apply build tools with Gradle, converting an existing Maven build to Gradle.

## Steps
### Create new branch
The first step was to create a new branch in the repository, which is used for the assignment, following the commands:

    git branch tut-basic-gradle
    git checkout tut-basic-gradle
    git push --set-upstream origin tut-basic-gradle

### Start new gradle spring project
Following the link _https://start.spring.io/_, a new _Gradle Project_ using the following dependencies, was generated
and downloaded:
* Rest Repositories
* Thymeleaf
* JPA
* H2

### Copy the previous project to folder ca2-part2 in the repository
The generated zip file was extracted to the _ca2-part2_ folder, and the default gradle tasks can be inspected by
executing `gradlew tasks`.

### Delete _src_ folder
The _src_ folder inside the demo spring application was deleted in the explorer

### Copy files from _ca1/basic_
The folder _src_ and files _webpack.config.js_ and _package.json_ was copied from the _ca1/basic_ folder to the empty
demo folder. Moreover, folder _src/main/resources/static/built was deleted.
 
### Run application
The application was executed with command `gradlew bootRun`, whose result is available at _http://localhost:8080/_.

### Add missing plugins
In the file _build.gradle_, add the `id "org.siouan.frontend" version "1.4.1"` to the plugins variable.

Add also the following piece of code to configure the previous plugin:

   ```
   frontend {
        nodeVersion = "12.13.1"
        assembleScript = "run webpack"
   }
   ```
        
### Update script
In the file _package.son_, add the following code block:

   ```
   "scripts": {
        "webpack": "webpack"
   },
   ```

### Run the application again
The application was executed with command `gradlew build`, followed by `gradlew bootRun`, whose result is available at _http://localhost:8080/_.

### Add task to copy jar folder
In the file _build.gradle_, add the following code block:

   ```
   task jarCopy(type: Copy) {
            from 'build/libs/demo-0.0.1-SNAPSHOT.jar'
            into 'dist'
   }
   ```

To run this task, we use the command `gradlew jarCopy`.

### Add task to delete automatic files
In the file _build.gradle_, add the following code block:

   ```
   clean.doFirst {
       delete 'src/main/resources/static/built/.'
   }
   ```

To run this task, we use the command `gradlew clean`.
       
### Merge branch
To upload the changes detailed before to the repository, we use the following commands:
    
    git add -A
    git commit -a -m "Adicionados ficheiros ca2-part2"
    git checkout master
    git merge tut-basic-gradle
    git push

### Closure
To finalize, a tag was was given to mark the assignment, and the present README.md were committed and pushed to the
repository: 

    git commit -a -m "Added final version of README.md for ca2-part2"
    git push
    git tag ca2-part2
    git push origin ca2-part2
